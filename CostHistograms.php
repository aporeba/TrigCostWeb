<?php
class CostHistograms {

  var $ROOTFile;
  var $directories = array();
  var $directoryStr;
  
  function __construct($ROOTFile) {
    $this->ROOTFile = $ROOTFile;
  }
  
  function addDirectory($dir) {
    $this->directories[] = $dir;
    $this->directoryStr .= $dir . "/";
  }

  function insertHistograms() {
echo <<< EXPORT
<p>
Histograms from ROOT file: <a href="{$this->ROOTFile}">{$this->ROOTFile}</a>,<br/>TDirectory: {$this->directoryStr}
</p>
<div id='msgRep'></div>
<div class='column'>
<input type='hidden' name='state' value='{$this->ROOTFile}' id='urlToLoad'/>
<div id='reportA'>
</div></div><div class='column'><div id='reportB'></div></div>
<script type="text/javascript">
// <![CDATA[

console.log("in script");
// Load ROOT js
window.onload = function () { 
  console.log("do load");
  ReadFile();
}  
    
var nav = [];

EXPORT;
  foreach ($this->directories as &$directory) {
    echo "nav.push('{$directory}');\n";
  }
echo <<< EXPORT
var readLevel = 0; // Current TDirectory search level
var readHist = -1; // Current histogram (-1 if still navigating directories)

// Use end of initial file read to trigger directory reading
function userCallback(file) {
  userReadKeyCallback(file); // Begin the directory search loop
}
    
// Use end of ReadKey to trigger reading next directory
function userReadKeyCallback(file) {
  console.log("reading at level " + readLevel);
  if (readLevel > nav.length - 1) { // If at bottom TDirectory
    if (readHist == -1) {
	    readHist = 0; // Begin the histogram output loop
      userObjBufferCallback(file);
    }
    return;
  }
  var keys = [];
  if (readLevel == 0) {
    keys = file.fKeys; // First call, root TDirectories from file
  } else {
    keys = file.fDirectories[readLevel-1].fKeys; // Latter call, TDirectories from current directory
  }
  var nKeys = keys.length - 1;
  var found = false;
  for (var i=nKeys; i >= 0; i--) {
//  if ( keys[i].name.indexOf( nav[readLevel] ) > -1 ) { // Check if key name contains search string
    if ( keys[i].name == nav[readLevel] ) { // Check if key name contains search string
      file.ReadDirectory(keys[i].name, keys[i].cycle, 0); // Read the directory
      found = true;
      break;
    }
  }
  if (found == false) infoMsg("TDirectory Not Found.");
  readLevel += 1;
}

// Use end of ObjectBuffer to trigger reading next histogram
function userObjBufferCallback(file) {
  console.log("histo at level " + readHist);
  if (readHist == -1) return; // If not yet at destination TDirectory
  var dirDepth = nav.length - 1; // Depth of final TDirectory
  var nHists = file.fDirectories[dirDepth].fKeys.length - 1; // N things to plot
  if (nHists == -1) {
    console.log("Warning! No histograms (" + nHists + ") found in directory");
    infoMsg("No Histograms Found In TDirectory.");
  }
  if (readHist > nHists) return;
  if (file.fDirectories[dirDepth].fKeys[readHist].name.indexOf("type") > -1) return; //This is a temp hack
  console.log("drawing " + file.fDirectories[dirDepth].fKeys[readHist].name);
  showObject( file.fDirectories[dirDepth].fKeys[readHist].name , 1);
  readHist += 1;
}

// ]]>
</script>
EXPORT;
  }
  
}
?>
