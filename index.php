<?php
error_reporting(E_ALL);
ini_set('memory_limit', '256M');
ini_set('display_errors', True);
include_once 'functions.php';
////////////////////////////////////////////////////////////////////////////////////////////
// These will be symlinked *NO UNDERSCORES IN KEYS HERE*
$dataDirs2020 = array( 
 // "Local" => "/data",
  "Technical-and-M-weeks" => "/data/Technical_and_M_weeks",
  "RateProcessings-LS2" => "/data/RateProcessings_LS2",
  "CostProcessings-LS2" => "/data/CostProcessings_LS2",
);
$dataDirs2018 = array( 
 // "Local" => "/data",
  "data18-13TeV" => "/data/data18_13TeV",
  "data18-hi" => "/data/data18_hi",
  "data18-cos" => "/data/data18_cos",
  "OnlineRate-2018" => "/data/OnlineRate_2018",
  "RateProcessings-2018" => "/data/RateProcessings_2018",
  "CostProcessings-2018" => "/data/CostProcessings_2018",
  "ART-TriggerTest-21.0" => "/eos/atlas/atlascerngroupdisk/trig-daq/ART",
  "ART-PrescalePhysicsV7Test-21.1" => "/afs/cern.ch/work/a/atrvshft/public/ART_trigCost",
);
$dataDirs2017 = array( 
 // "Local" => "/data",
  "data17-13TeV" => "/data/data17_13TeV",
  "data17-cos" => "/data/data17_cos",
  "OnlineRate-2017" => "/data/OnlineRate_2017",
  "RateProcessings-2017" => "/data/RateProcessings_2017",
  "CostProcessings-2017" => "/data/CostProcessings_2017",
);
$dataDirs2016 = array( 
 // "Local" => "/data",
  "data16-13TeV" => "/data/data16_13TeV",
  "data16-hip8TeV" => "/data/data16_hip8TeV/",
  "data16-hip5TeV" => "/data/data16_hip5TeV/",
  "data16-cos" => "/data/data16_cos",
  "OnlineRate-2016" => "/data/OnlineRate_2016",
  "RateProcessings-2016" => "/data/RateProcessings_2016",
  "CostProcessings-2016" => "/data/CostProcessings_2016",
);
$dataDirs2015 = array( 
 // "Local" => "/data",
  "data15-13TeV" => "/data/data15_13TeV",
  "data15-5TeV" => "/data/data15_5TeV",
  "data15-hi" => "/data/data15_hi",
  "data15-cos" => "/data/data15_cos",
  "RateProcessings-2015" => "/data/RateProcessings",
  "CostProcessings-2015" => "/data/CostProcessings",
  "M8-M9" => "/data/M8-M9",
  "M7" => "/data/M7",
  "M6" => "/data/M6",
);
$otherLinks = array(
  "online" => '/data/online_run1',
  "RateProcessings" => "/data/RateProcessings",
  "CostProcessings" => "/data/CostProcessings",
);
//////////////////////////////////////////////////////////////////////////////////////////
$TT_ARTPath_T0 = "/21.0/*/Athena/x86_64-slc6-gcc*-opt/TriggerTest/";
$dirPostPatters = array( // This holds additional file paths needed to grep for runs
  "ART-TriggerTest-21.0" => array(
    "test_mc_hi_v3_tight_grid" => "{$TT_ARTPath_T0}test_mc_hi_v3_tight_grid",
    "test_mc_hi_v4_grid" => "{$TT_ARTPath_T0}test_mc_hi_v4_grid",
    "test_mc_pp_v7_grid" => "{$TT_ARTPath_T0}test_mc_pp_v7_grid",
    "test_mc_pp_v7_loose_grid" => "{$TT_ARTPath_T0}test_mc_pp_v7_loose_grid",
    "test_mc_pp_v7_nops_aod_ftk_grid" => "{$TT_ARTPath_T0}test_mc_pp_v7_nops_aod_ftk_grid",
    "test_mc_pp_v7_tight_grid" => "{$TT_ARTPath_T0}test_mc_pp_v7_tight_grid",
    "test_physics_hi_v4_grid" => "{$TT_ARTPath_T0}test_physics_hi_v4_grid",
    "test_physics_pp_v7_aod_grid" => "{$TT_ARTPath_T0}test_physics_pp_v7_aod_grid",
    "test_physics_pp_v7_grid" => "{$TT_ARTPath_T0}test_physics_pp_v7_grid",
    "test_physics_pp_v7_tight_grid" => "{$TT_ARTPath_T0}test_physics_pp_v7_tight_grid",
    "test_slice_bjet_grid" => "{$TT_ARTPath_T0}test_slice_bjet_grid",
    "test_slice_bphysics_aod_grid" => "{$TT_ARTPath_T0}test_slice_bphysics_aod_grid",
    "test_slice_bphysics_grid" => "{$TT_ARTPath_T0}test_slice_bphysics_grid",
    "test_slice_bphysicsexo_aod_grid" => "{$TT_ARTPath_T0}test_slice_bphysicsexo_aod_grid",
    "test_slice_electron_grid" => "{$TT_ARTPath_T0}test_slice_electron_grid",
    "test_slice_jet_grid" => "{$TT_ARTPath_T0}test_slice_jet_grid",
    "test_slice_l1_grid" => "{$TT_ARTPath_T0}test_slice_l1_grid",
    "test_slice_met_grid" => "{$TT_ARTPath_T0}test_slice_met_grid",
    "test_slice_minbias_grid" => "{$TT_ARTPath_T0}test_slice_minbias_grid",
    "test_slice_minbias_hmt_grid" => "{$TT_ARTPath_T0}test_slice_minbias_hmt_grid",
    "test_slice_muon_grid" => "{$TT_ARTPath_T0}test_slice_muon_grid",
    "test_slice_tau_grid" => "{$TT_ARTPath_T0}test_slice_tau_grid",
  ),
  "ART-PrescalePhysicsV7Test-21.1" => array(
    "prescaled_1700" => "/prescaled_17000",
    "Physics_pp_v7_unprescaled_only_physics_prescale" => "/Physics_pp_v7_unprescaled_only_physics_prescale",
  ),
);
populateSubdirectoryArray("RateProcessings","/data/RateProcessings");
populateSubdirectoryArray("RateProcessings-2015","/data/RateProcessings");
populateSubdirectoryArray("RateProcessings-2016","/data/RateProcessings_2016");
populateSubdirectoryArray("CostProcessings-2016","/data/CostProcessings_2016");
populateSubdirectoryArray("OnlineRate-2016","/data/OnlineRate_2016");
populateSubdirectoryArray("RateProcessings-2017","/data/RateProcessings_2017");
populateSubdirectoryArray("CostProcessings-2017","/data/CostProcessings_2017");
populateSubdirectoryArray("OnlineRate-2017","/data/OnlineRate_2017");
populateSubdirectoryArray("RateProcessings-2018","/data/RateProcessings_2018");
populateSubdirectoryArray("CostProcessings-2018","/data/CostProcessings_2018");
populateSubdirectoryArray("OnlineRate-2018","/data/OnlineRate_2018");
////////////////////////////////////////////////////////////////////////////////////////
$levels = array('HLT','EF','L2');
$runRanges = array('All.', 'LumiBlock', 'SM'); // Event handleded on its own. NOTE: was "All_" why was it like this?
// Note: Full_Evnt handleded on its own.
$algSummaries = array('Algorithm', 'Algorithm_Class', 'Sequence', 'Sequence_Algorithm', 'Chain', 'Chain_Algorithm');
$rateSummaries = array('Rate_Upgrade_ChainL0', 'Rate_ChainL1', 'Rate_Upgrade_ChainL1', 'Rate_ChainHLT', 'Rate_Upgrade_ChainHLT', 'Rate_Combination', 'Rate_Group', 'Rate_Upgrade_Group', 'Rate_Upgrade_Combination', 'Rate_Unique');
$ROSSummaries = array('ROS', 'ROS_Algorithm', 'ROS_Chain', 'ROBIN');
$profileSummaries = array('EventProfile_AllEvents', 'EventProfile_Muon', 'EventProfile_JetFullScan', 'EventProfile_IDFullScan');
$otherSummaries = array('ROI', 'Global', 'SliceCPU', 'Thread_Occupancy');
$summaryList = array( 'Chain, Sequence and Algorithm' => $algSummaries,
                      'Rate' => $rateSummaries,
                      'Read Out System' => $ROSSummaries,
                      'Event Profiles' => $profileSummaries,
                      'Other' => $otherSummaries);

$separator = '____'; // :/
////////////////////////////////////////////////////////////////////////////////////////
// Globals
$page = isset($_GET['page']) ? sanitise($_GET['page']) : NULL;
$key = isset($_GET['key']) ? sanitise($_GET['key']) : NULL;
$dir = isset($_GET['dir']) ? sanitise($_GET['dir']) : NULL;
$type = isset($_GET['type']) ? sanitise($_GET['type']) : ""; // Note type is optional - it defaults to empty string
$tag = isset($_GET['tag']) ? sanitise($_GET['tag']) : NULL;
$run = isset($_GET['run']) ? sanitise($_GET['run']) : NULL;
$range = isset($_GET['range']) ? sanitise($_GET['range']) : NULL;
$rangeDisplay = str_replace("_"," ",$range);
$config = isset($_GET['config']) ? sanitise($_GET['config']) : NULL;
$configDisplay = str_replace("_"," ",$config);
$search = isset($_GET['search']) ? sanitise($_GET['search']) : NULL;
$level = isset($_GET['level']) ? sanitise($_GET['level']) : NULL;
$summary = isset($_GET['summary']) ? sanitise($_GET['summary']) : NULL;
$summaryDisplay = str_replace("_"," ",$summary);
$item = isset($_GET['item']) ? sanitise($_GET['item']) : NULL;
$addLink = isset($_GET['addLink']) ? sanitise($_GET['addLink']) : NULL;
$rmLink = isset($_GET['rmLink']) ? sanitise($_GET['rmLink']) : NULL;
$theme = isset($_GET['theme']) ? sanitise($_GET['theme']) : NULL;
$USER = isset($_SERVER['OIDC_CLAIM_name']) ? sanitise($_SERVER['OIDC_CLAIM_name']) : 'Test User';
$USERNAME = isset($_SERVER['OIDC_CLAIM_preferred_username']) ? sanitise($_SERVER['OIDC_CLAIM_preferred_username']) : 'testuser';
$ROLES = isset($_SERVER['OIDC_CLAIM_cern_roles']) ? sanitise($_SERVER['OIDC_CLAIM_cern_roles']) : 'users';
////////////////////////////////////////////////////////////////////////////////////////
// Admins
$isAdmin = (strpos($ROLES, "admin") !== FALSE);
$isExpert = $isAdmin ? TRUE : (strpos($ROLES, "expert") !== FALSE);
$isDark = ($theme == 'dark');
////////////////////////////////////////////////////////////////////////////////////////
$err = array();
$strictXHTML = TRUE; // :)
$strictCSS = TRUE;  // :)
include_once 'content.php';
if (isset($summary)) include_once 'CostDataTable.php';
if (isset($item))    include_once 'CostHistograms.php';
if (isset($config) || isset($item)) {
  include_once 'ConfigurationTable.php';
  include_once 'RatesGraph.php';
}
if (isset($page) and $page == "CompareRates") {
  include_once 'CostDataTable.php';
  include_once 'CompareRates.php';
}
if (isset($page) and $page == "ProcessingRequest") include_once 'ProcessingRequest.php';
////////////////////////////////////////////////////////////////////////////////////////

// Parse GET information. Retrieve lists of data.
// Check we have the capacity to display all that has been requested.
$detailLevel = getDetailLevel();
// Check if the user has asked us to add or remove any symlinks or if we are missing the base links
checkSymLinks();

// Begin page
echoPageHeader("Trigger Cost Browser");
echoErrorMsgs();


// Title header
if ($detailLevel == 1) echoHeader(1, "Directory Summary");
else if ($detailLevel == 2) echoHeader(1, "Run Summary");
else if ($detailLevel == 3 && isset($range)) echoHeader(1, "Run Ranges Summary");
else if ($detailLevel == 3 && isset($config)) echoHeader(1, "Run Configuration Summary");
else if ($detailLevel == 3 && isset($page) && $page == 'metadata') echoHeader(1, "Run Metadata");
else if ($detailLevel == 4) echoHeader(1, "Display {$summaryDisplay} Summary");
else if ($detailLevel == 5) echoHeader(1, "Display {$item} Summary");
else echoHeader(1, "Trigger Cost Browser");
echo "<hr/>";

// Breadcrumb trail
echoBreadcrumb();
echo "<hr/>";

echo "<!-- detailLevel is {$detailLevel} -->";

//Content
if ($detailLevel == 0) {
  content_homePage();
} else if ($detailLevel == 1) {
  content_displayRuns();
} else if ($detailLevel == 2) {
  content_displayRunRanges();
} else if ($detailLevel == 3 && isset($range)) {
  content_displayRangeSummaries();
} else if ($detailLevel == 3 && isset($config)) {
  content_displayConfiguration();
} else if ($detailLevel == 3 && isset($page) && $page == 'metadata') {
  content_displayMetadata();
} else if ($detailLevel == 4) {
  content_displaySummary();
} else if ($detailLevel == 5) {
  content_displayItem();
} else if ($detailLevel == 100 && $page == 'About') {
  page_about();
} else if ($detailLevel == 100 && $page == 'symlinks' && $isAdmin == TRUE) {
  page_symlinks();
} else if ($detailLevel == 100 && ($page == 'log' or $page == 'logManual') && ($isAdmin == TRUE or $isExpert == TRUE) ) {
  page_log();
} else if ($detailLevel == 100 && $page == 'sw' && ($isAdmin == TRUE or $isExpert == TRUE) ) {
  page_software();
} else if ($detailLevel == 100 && $page == 'CompareRates') {
  page_compareRates();
} else if ($detailLevel == 100 && $page == 'ProcessingRequest' && ($isAdmin == TRUE or $isExpert == TRUE) ) {
  page_processingRequest();
} else if ($detailLevel == 100 && $page == 'phpinfo') {
  echo "<br/>  /afs/cern.ch/atlas/project/RTT/prod/Results/rtt = ";
  print_r( glob( "/afs/cern.ch/atlas/project/RTT/prod/Results/rtt") );

  echo "<br/>  /eos/ = ";
  print_r( glob( "/eos/") );

  echo "<br/>  /eos/atlas = ";
  print_r( glob( "/eos/atlas/") );
  
  echo "<br/>  /eos/atlas/atlascerngroupdisk/trig-daq/ART/21.0/2018-01-08T2153/Athena/x86_64-slc6-gcc62-opt/TriggerTest/test_physics_pp_v7_grid = ";
  print_r( glob( "/eos/atlas/atlascerngroupdisk/trig-daq/ART/21.0/2018-01-08T2153/Athena/x86_64-slc6-gcc62-opt/TriggerTest/test_physics_pp_v7_grid") );
  phpinfo();
} else {
  page_unknown();
}

echoFooter();

?>
