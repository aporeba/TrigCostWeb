# This script runs the analysis

################################################## ##################################################
HOME=/opt/trig-cost-sw
################################################## ##################################################

echo "Execute] Manual invoked cost processing"

# Go home
cd $HOME

#setup eos
echo "Execute] Setup EOS"
#export PATH=$PATH:/afs/cern.ch/project/eos/installation/pro/bin/
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/cern.ch/project/eos/installation/pro/lib64/
export EOS_MGM_URL=root://eosatlas.cern.ch

kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab # Authenticate

# Setup release
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
echo "Execute] Source atlasLocalSetup.sh"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo "Execute] Setup athena"
source ${AtlasSetup}/scripts/asetup.sh AthenaP1,21.1.41

# Prepare run command
RUN=`cat $1`

echo "Execute] COMMAND: $RUN"
$RUN

# Check straight away for another thing to process
echo "breakSleep" > /var/www/TrigCostWeb/breakSleep

