<?php 

function page_processingRequest() {
  global $err, $isAdmin;

  $filesRoot = "/var/www/TrigCostWeb/filelists/";
  $commandsRoot = "/var/www/TrigCostWeb/commands/";
  $costRoot = "/data/CostProcessings_2018/";
  $ratesRoot = "/data/RateProcessings_2018/";
  $timestamp = time();

  // When doing a cost processing, which LB do we start on?
  // This is also how we tell if a run is EnhancedBias or not
  $arrayCostLBStart = array();
  $arrayCostLBStart[266904] = 410;
  $arrayCostLBStart[267638] = 400;
  $arrayCostLBStart[271421] = 305;
  $arrayCostLBStart[276952] = 120;
  $arrayCostLBStart[280500] = 130;
  $arrayCostLBStart[286665] = 210; // HI
  $arrayCostLBStart[286717] = 400; // HI
  $arrayCostLBStart[287843] = 230; // HI
  $arrayCostLBStart[298967] = 170;
  $arrayCostLBStart[296939] = 170;
  $arrayCostLBStart[302956] = 260;
  $arrayCostLBStart[307126] = 145;
  $arrayCostLBStart[309640] = 628;
  $arrayCostLBStart[310574] = 114;
  $arrayCostLBStart[218048] = 529; // pPb overlay
  $arrayCostLBStart[312649] = 119; // pPb 5 TeV
  $arrayCostLBStart[312796] = 170; // pPb 5 TeV
  $arrayCostLBStart[312945] = 20; // pPb 5 TeV
  $arrayCostLBStart[313063] = 12; // pPb 8 TeV
  $arrayCostLBStart[327265] = 81;
  $arrayCostLBStart[334443] = 85;
  $arrayCostLBStart[339070] = 78;
  $arrayCostLBStart[339849] = 138;
  $arrayCostLBStart[341615] = 550;
  //2018
  $arrayCostLBStart[349335] = 191;
  $arrayCostLBStart[360026] = 125;
  $arrayCostLBStart[360129] = 100;
  $arrayCostLBStart[365573] = 210;
  $arrayCostLBStart[367273] = 150; // PbPb
  $arrayCostLBStart[367365] = 140; // PbPb UPC

  // And which LB do we end on?
  $arrayCostLBEnd = array();
  $arrayCostLBEnd[266904] = 420;
  $arrayCostLBEnd[267638] = 410;
  $arrayCostLBEnd[271421] = 315;
  $arrayCostLBEnd[276952] = 130;
  $arrayCostLBEnd[280500] = 140;
  $arrayCostLBEnd[286665] = 229; // HI
  $arrayCostLBEnd[286717] = 419; // HI
  $arrayCostLBEnd[287843] = 239; // HI
  $arrayCostLBEnd[298967] = 179;
  $arrayCostLBEnd[296939] = 179;
  $arrayCostLBEnd[302956] = 264;
  $arrayCostLBEnd[307126] = 150;
  $arrayCostLBEnd[309640] = 631;
  $arrayCostLBEnd[310574] = 116;
  $arrayCostLBEnd[218048] = 1112; // pPb overlay
  $arrayCostLBEnd[312649] = 194; // pPb 5 TeV
  $arrayCostLBEnd[312796] = 180; // pPb 5 TeV
  $arrayCostLBEnd[312945] = 30; // pPb 5 TeV
  $arrayCostLBEnd[313063] = 22; // pPb 8 TeV
  $arrayCostLBEnd[327265] = 85; // pPb 8 TeV
  $arrayCostLBEnd[327265] = 87;
  $arrayCostLBEnd[334443] = 91;
  $arrayCostLBEnd[339070] = 85;
  $arrayCostLBEnd[339849] = 155;
  $arrayCostLBEnd[341615] = 560;
  // 2018
  $arrayCostLBEnd[349335] = 195;
  $arrayCostLBEnd[360026] = 130;
  $arrayCostLBEnd[360129] = 101;
  $arrayCostLBEnd[365573] = 215;
  $arrayCostLBEnd[367273] = 250; // PbPb
  $arrayCostLBEnd[367365] = 400; // PbPb UPC

  // First submit data
  $files = isset($_POST['files']) ? sanitise($_POST['files']) : NULL;
  $nFiles = 0;
  $doRates = isset($_POST['doRates']) ? TRUE : FALSE;
  $doCost = isset($_POST['doCost']) ? TRUE : FALSE;
  $doValidate = isset($_POST['doValidate']) ? TRUE : FALSE;
  $skipValidate = (isset($_POST['skipValidate']) and $isAdmin == TRUE) ? TRUE : FALSE;
  $prescaleXML = isset($_POST['prescaleXML']) ? sanitise($_POST['prescaleXML']) : NULL;
  $JIRA = isset($_POST['JIRA']) ? sanitise($_POST['JIRA']) : NULL;
  $JIRAFull = NULL;
  $release = isset($_POST['release']) ? sanitise($_POST['release']) : NULL;
  $runNumber = isset($_POST['runNumber']) ? intval(sanitise($_POST['runNumber'])) : NULL;
  $description = isset($_POST['description']) ? sanitise($_POST['description']) : NULL;

  // Second submit data
  $commandRates = isset($_POST['commandRates']) ? sanitise($_POST['commandRates']) : NULL;
  $commandCost = isset($_POST['commandCost']) ? sanitise($_POST['commandCost']) : NULL;
  $filesSubmit = isset($_POST['filesSubmit']) ? sanitise($_POST['filesSubmit']) : NULL;
  $inputFileListName = isset($_POST['inputFileListName']) ? sanitise($_POST['inputFileListName']) : NULL;
  $ratesCommandFile = isset($_POST['ratesCommandFile']) ? sanitise($_POST['ratesCommandFile']) : NULL;
  $costCommandFile = isset($_POST['costCommandFile']) ? sanitise($_POST['costCommandFile']) : NULL;
  $doSubmit = isset($_POST['doSubmit']) ? TRUE : FALSE;

  $status = "unvalidated";

  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  if ($skipValidate == TRUE) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $status = "validated"; // Yeah, it's fine I'm sure
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  } else if ($doValidate == TRUE) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // CHECK INPUT FILES
    if ($files == NULL or $files == "") {
      $err[] = "ERROR: No files supplied";
    }
    $fileArray = explode ("\n", $files);
    $nFiles = count($fileArray);
    $hasCostFiles = FALSE;
    $hasRateFiles = FALSE;
    foreach ($fileArray as $line) {
      if (substr($line,0,5) != "/afs/" and substr($line,0,5) != "/eos/" and substr($line,0,7) != "/cvmfs/" and substr($line,0,21) != "root://eosatlas//eos/") {
        $err[] = "ERROR: Invalid input file '{$line}'";
        break;
      };
      // Work out the file type
      if (strpos($line, "NTUP_TRIGCOST") !== FALSE or strpos($line, "trig_cost") !== FALSE) {
        $hasCostFiles = TRUE;
      } else if (strpos($line, "NTUP_TRIGRATE") !== FALSE) {
        $hasRateFiles = TRUE;
      } else {
        $err[] = "ERROR: First input file name must contain either 'NTUP_TRIGCOST', 'NTUP_TRIGRATE', 'trig_cost' or 'trig_rate'.";
        break;
      }
    }

    // CHECK TYPE
    if ($doRates == FALSE and $doCost == FALSE) $err[] = "ERROR: Must choose at least one of Rates or Cost mode";
    if ($doCost == TRUE and $hasCostFiles == FALSE) $err[] = "ERROR: Cannot do a cost processing without 'NTUP_TRIGCOST' input file(s).";
    if ($doRates == TRUE and $hasCostFiles == FALSE and $hasRateFiles == FALSE) $err[] = "ERROR: Cannot do a cost processing without 'NTUP_TRIGRATE' or 'NTUP_TRIGCOST' input file(s).";
    if ($hasCostFiles == TRUE and $hasRateFiles == TRUE) $err[] = "ERROR: Specified both 'NTUP_TRIGRATE' AND 'NTUP_TRIGCOST' input file(s). Cannot use both. Please use 'NTUP_TRIGCOST' for COST and/or RATES processing or 'NTUP_TRIGRATE' for rates only.";
    if ($doRates == TRUE and array_key_exists($runNumber, $arrayCostLBStart) == FALSE) {
      $err[] = "ERROR: Rates mode requires EnhancedBias data, this script is currently unaware of an EB run '{$runNumber}'. Please contact atlas-trigger-rate-expert to update it.";
    }

    // CHECK PRESCALE XML IS VALID
    if ($prescaleXML != NULL) {
      if (is_readable($prescaleXML) == FALSE) $err[] = "ERROR: Supplied prescaleXML file '{$prescaleXML}' can not be found or is not readable.";
    }

    // CHECK JIRA
    if ($JIRA != NULL) {
      if (is_numeric($JIRA) == false) $err[] = "ERROR: Invalid JIRA given. Must be a number.";
      else $JIRAFull = "ATR-" . $JIRA;
    } 

    // CHECK RELEASE
    if ($release == NULL) {
      $err[] = "ERROR: Release number must be supplied.";
    } else {
      $releaseExplode = explode (".", $release);
      $releaseStyle = true;
      foreach ($releaseExplode as $element) {
        if (is_numeric($element) === FALSE) {
          $releaseStyle = false;
          break;
        }
      }

      if ($releaseStyle === FALSE && DateTime::createFromFormat('?Y#m#d', $release) === FALSE) { // Test time style
        $err[] = "ERROR: Invalid release number given. Should numbers separated by full-stops or of the format rYYYY-MM-DD";
      }

    }

    // CHECK DESCRIPTION
    if ($description != NULL) {
      if (strpos($description, " -") !== FALSE) $err[] = "ERROR: Description may not contain the pattern ' -'.";
    }

    if(!empty($err)) echoErrorMsgs();
    else $status = "validated";
    
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  } else if ( $doSubmit == TRUE ) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // MAKE A LOOSER CHECK OF THE RESUBMITTED DATA
    if ($filesSubmit == NULL) $err[] = "ERROR: Did not receive files.";
    if ($inputFileListName == NULL) $err[] = "ERROR: File path was not transmitted.";
    if ($commandRates == NULL and $commandCost == NULL) $err[] = "ERROR: Did not receive command to execute.";
    if ($commandRates != NULL and $ratesCommandFile == NULL) $err[] = "ERROR: Did not receive path to save rates command to.";
    if ($commandCost  != NULL and $costCommandFile == NULL) $err[] = "ERROR: Did not receive path to save cost command to.";
    if(!empty($err) and $isAdmin == FALSE) { // Admins get to skip this validation too as they can fix anything which goes wrong
      $err[] = "ERROR: Some data was not received. Please go back one page and make sure no text boxes are left empty";
      $status = "err"; // This will mean we don't show anything else below
    } else {
      $status = "submit";
    }
    echoErrorMsgs();


  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  }
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  // IF VALIDATED - SHOW PROTO-COMMAND
  /////////////////////////////////////////////////////////////////////////////////////////////
  if ($status == "validated") {
  /////////////////////////////////////////////////////////////////////////////////////////////
    
    $inputFileListName = $filesRoot . "files_" . $JIRAFull . "_" . $timestamp . ".txt";
    // Any user details?
    $userDetails = "";
    if ($description != NULL) $userDetails = "--userDetails " . $description;
    // Any JIRA?
    $jiraDetails = "";
    if ($JIRAFull != NULL) $jiraDetails = "--jira " . $JIRAFull; 
    // OUTPUT FILE INFO
    echo "<form action='?page=ProcessingRequest' method='post'>";
    echo "<input type='hidden' name='inputFileListName' value='{$inputFileListName}' />";
    echo "<h2>Input Files Summary</h2>";
    echo "<div class='subtitle'>{$nFiles} files are to be processed.</span></div>";
    echo "<p><textarea name='filesSubmit' rows='10' cols='150'>{$files}</textarea></p>";
    // COST MODE
    if ($doCost == TRUE) {
      echo "<h2>Cost Processing Command Summary</h2>";
      $outputCostDir = $costRoot . $release . "/";
      // Work out where the command will be written
      $costCommandFile = $commandsRoot . "cost_" . $JIRAFull . "_" . $timestamp . ".txt";
      echo "<input type='hidden' name='costCommandFile' value='{$costCommandFile}' />";
      // Work out an output tag name
      $costTag = "cost-reprocessing-";
      if ($JIRAFull != NULL) $costTag .= $JIRAFull;
      else $costTag .= $timestamp;
      // Is this EB data? If so we want to specify the range and do weighting
      $EBCost = "";
      if (array_key_exists($runNumber, $arrayCostLBStart)) {
        $EBCost = "--lbBegin {$arrayCostLBStart[$runNumber]} --lbEnd {$arrayCostLBEnd[$runNumber]} --useEBWeight";
        echo "<div class='subtitle'>Cost processing over a EnhancedBias data run. The '<span class='codePara'>--useEBWeight</span>' option and a good range of LB have been selected.</div>";
      } else {
        echo "<p><span class='processing'>WARNING: Cost analysis of reprocessing is CPU intensive. A standard range of LB to process is not known for run {$runNumber}.<span></p>";
        echo "<p><span class='processing'>It is strongly recommended you specify a small range (5 to 20) of good LB from this run by adding to the command '--lbBegin XXX --lbEnd XXX'.</span></p>";
      }
      // Construct Command
      $commandCost = "RunTrigCostD3PD --costMode --messageWait 180 --fileList {$inputFileListName} --outputRootDirectory {$outputCostDir} --outputTag {$costTag} {$userDetails} {$jiraDetails} {$EBCost}";
      echo "<p><textarea name='commandCost' rows='3' cols='150'>{$commandCost}</textarea></p>";
    }
    if ($doRates == TRUE) {
      echo "<h2>Rates Processing Command Summary</h2>";
      $outputRateDir = $ratesRoot . $release . "/";
      // Work out where the command will be written
      $ratesCommandFile = $commandsRoot . "rate_" . $JIRAFull . "_" . $timestamp . ".txt";
      echo "<input type='hidden' name='ratesCommandFile' value='{$ratesCommandFile}' />";
      // Work out an output tag name
      $rateTag = "rate-prediction-";
      if ($JIRAFull != NULL) $rateTag .= $JIRAFull . "-";
      if ($prescaleXML != NULL) $rateTag .= basename($prescaleXML);
      else $rateTag .= "noPS";
      // prescales?
      $prescaleDetails = "";
      if ($prescaleXML != NULL) $prescaleDetails = "--prescaleXML " . $prescaleXML;
      // Construct Command
      // pp: --targetMu 60 --predictionLumi 2e34
      // PbPb: --predictionLumi 3e27
      $commandRates = "RunTrigCostD3PD --ratesMode --targetMu 60 --predictionLumi 2e34 --messageWait 90 --fileList {$inputFileListName} --outputRootDirectory {$outputRateDir} --outputTag {$rateTag} {$userDetails} {$jiraDetails} {$prescaleDetails}";
      echo "<p><textarea name='commandRates' rows='3' cols='150'>{$commandRates}</textarea></p>";
    }
    // final submit button
    echo "<h2>Submit</h2>";
    echo "<div class='subtitle'>Commands have been generated from your input, please check them and make any alterations which may be required before submitting the request.</div>";
    echo "<input type='submit' value='Queue Processing' name='doSubmit'/>";
    echo "</form>";

  /////////////////////////////////////////////////////////////////////////////////////////////    
  } else if ($status == "submit") {
  /////////////////////////////////////////////////////////////////////////////////////////////

    // DO THE SUBMISSION AND SHOW THE SUMMARY
    echo "<form action='?page=ProcessingRequest' method='post'>"; // We never actually submit this
    if ($filesSubmit != NULL and $inputFileListName != NULL) {
      echo "<h2>Input Files</h2>";
      $filesSubmit = str_replace("\r", "", $filesSubmit); // We need unix line breaks!
      if (file_put_contents($inputFileListName, $filesSubmit) === FALSE) echo "<p><span class='processing'>Error writing '<span class='codePara'>{$inputFileListName}</span>'!</span></p>";
      else echo "<p>File list written to: '<span class='codePara'>{$inputFileListName}</span>'</p>";
      echo "<p><textarea disabled name='filesSubmit' rows='10' cols='150'>{$filesSubmit}</textarea></p>";
    }

    if ($commandCost != NULL and $costCommandFile != NULL) {
      echo "<h2>Cost Processing</h2>";
      if (file_put_contents($costCommandFile, $commandCost) === FALSE) echo "<p><span class='processing'>Error writing '<span class='codePara'>{$costCommandFile}</span>'!</span></p>";
      else echo "<p>Command written to: '<span class='codePara'>{$costCommandFile}</span>'</p>";
      echo "<p><textarea disabled name='commandCost' rows='3' cols='150'>{$commandCost}</textarea></p>";
    }

    if ($commandRates != NULL and $ratesCommandFile != NULL) {
      echo "<h2>Rate Processing</h2>";
      if (file_put_contents($ratesCommandFile, $commandRates) === FALSE) echo "<p><span class='processing'>Error writing '<span class='codePara'>{$ratesCommandFile}</span>'!</span></p>";
      else echo "<p>Command written to: '<span class='codePara'>{$ratesCommandFile}</span>'</p>";
      echo "<p><textarea disabled name='commandRates' rows='3' cols='150'>{$commandRates}</textarea></p>";
    }

    echo "<h2>Submission Finished</h2>";
    echo "<p>When not processing other manual requests, the <span class='codePara'>atlas-trig-cost</span> VM should notice this request within 60 seconds.</p>";
    echo "<p>To view the VMs processing status please visit the <a href='" . getLinkPage("logManual") . "'><strong>logs</strong></a> page.</p>";
    echo "<p>Any mistakes or problems? Contact <strong>atlas-trigger-rate-expert@cern.ch</strong>.</p>";
    echo "</form>"; // We never actually submit this
    // This file tells the trig cost to wake from sleep
    file_put_contents("/var/www/TrigCostWeb/breakSleepManual", "breakSleepManual");

  /////////////////////////////////////////////////////////////////////////////////////////////
  } else if ($status == "unvalidated") { // Show data collection window
  /////////////////////////////////////////////////////////////////////////////////////////////

    $rateChecked = (($doRates == TRUE or $_SERVER['REQUEST_METHOD'] != 'POST') ? "checked='checked'" : "");
    $costChecked = (($doCost  == TRUE or $_SERVER['REQUEST_METHOD'] != 'POST') ? "checked='checked'" : "");
    $adminSkipValidation = ($isAdmin == TRUE ? "<p><input type='submit' value='Skip Validation [ADMIN]' name='skipValidate'/></p>" : "");

echo <<< EXPORT
<p>This interface allows trigger experts to schedule the processing of full <span class='codePara'>NTUP_TRIGCOST</span> or slimmed <span class='codePara'>NTUP_TRIGRATE</span> ntuples on the <span class='codePara'>atlas-trig-cost</span> virtual machine.</p>

<form action='?page=ProcessingRequest' method='post'>
  
  <h2>Input Files</h2>
  <div class='subtitle'>One file per line.</div>
  <div class='subtitle'>Must be accessible at CERN, allowed paths start with:  <span class='codePara'>/afs/, /cvmfs/, /eos/, root://eosatlas//eos/</span></div>
  <div class='subtitle'>Suggest grid command to find files at CERN <strong>(COST files are sometimes on CERN-PROD_DATADISK)</strong>: <span class='codePara'>rucio list-file-replicas --rse CERN-PROD_TRIG-HLT <i>DATASET_NAME_HERE</i> | grep -oE /eos/.*\.root\.1</span></div>
  <p><textarea name='files' rows='10' cols='150'>{$files}</textarea></p>

  <h2>Processing Type</h2>
  <div class='subtitle'>Cost processing (CPU time) requires <span class='codePara'>COST</span> ntuples, rate processing can use either <span class='codePara'>RATE</span> or <span class='codePara'>COST</span> ntuples.</div>
  <div class='subtitle'>Cost processing may be done for any run. Rates can only be done for reprocessed EnhancedBias data.</div>
  <p><input type='checkbox' name='doRates' value='true' {$rateChecked} />Rates Processing (from NTUP_TRIGCOST <i>or</i> NTUP_TRIGRATE files)</p>
  <p><input type='checkbox' name='doCost' value='true' {$costChecked} />Cost Processing (from NTUP_TRIGCOST files)</p> 

  <h2>Prescales To Apply to Rate Processing (Optional)</h2>
  <div class='subtitle'>A RuleBook prescale XML may be supplied if desired. See <a href='https://twiki.cern.ch/twiki/bin/view/Atlas/RunningTriggerCostEstimations' class='linkExternal'>RunningTriggerCostEstimations</a>.</div>
  <div class='subtitle'>Provide full path to prescale XML file, it must be in a public folder.</div>
  <p>Prescales XML Location: <input type='text' name='prescaleXML' value='{$prescaleXML}' maxlength='200' /></p>

  <h2>JIRA Link (Optional)</h2>
  <div class='subtitle'>Enter any trigger JIRA bug/task number for easier reference.</div>
  <p>ATR-<input type='text' name='JIRA' value='{$JIRA}' maxlength='10' /></p>

  <h2>Run Number</h2>
  <p>Run Number:<input type='text' name='runNumber' value='{$runNumber}' maxlength='15' /></p>

  <h2>Release Number</h2>
  <div class='subtitle'>Enter the release or nightly used to run the trigger e.g. <span class='codePara'>21.1.1</span> or <span class='codePara'>r2017-04-03</span>.</div>
  <p>Release:<input type='text' name='release' value='{$release}' maxlength='15' /></p>

  <h2>Description</h2>
  <div class='subtitle'>Please briefly describe the request. What was aim of the reprocessing? Were rereunL1 or applyL1 modifiers used? Was the TriggerMenu tag non-standard? Which menu was used?</div>
  <p>Description:<input type='text' name='description' value='{$description}' maxlength='400'/></p>

  <h2>Check Submission</h2>
  <p><input type='submit' value='Validate' name='doValidate'/></p>
  {$adminSkipValidation}

</form>

EXPORT;
  /////////////////////////////////////////////////////////////////////////////////////////////
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}


?>
